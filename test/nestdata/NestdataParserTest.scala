package nestdata

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._

import scala.io.Source

// Don't need this for unit test
// with GuiceOneAppPerTest with Injecting {

//class NestdataParserTest extends PlaySpec :
//
//  "Parsers" should {
//
//    "read whitespace" in {
//      val in = Source.fromString("   |   ")
//      val p = new NestdataParser(in)
//      val pw = new p.ParseWhitespace
//
//      pw.read mustBe Some(())
//      p.next mustBe Some('|')
//    }
//    
//    "read a name" in {
//      val in = Source.fromString("zing|   ")
//      val p = new NestdataParser(in)
//      val pn = new p.ParseName
//
//      pn.read mustBe Some("zing")
//      p.next mustBe Some('|')
//    }
//  
//    "read a value" in :
//      val in = Source.fromString("blammo_&|    \n   ")
//      val p = new NestdataParser(in)
//      val pn = new p.ParseValue
//
//      pn.read mustBe Some("blammo_&|")
//      p.next mustBe Some('\n')
//
//    "read a quoted value" in {
//      val in = Source.fromString("'blammo_&'|    \n   ")
//      val p = new NestdataParser(in)
//      val pn = new p.ParseQuotedValue
//
//      pn.read mustBe Some("blammo_&")
//      p.next mustBe Some('|')
//    }
//
//    "read a quoted value until newline" in {
//      val in = Source.fromString("'blammo_&|    \n   ")
//      val p = new NestdataParser(in)
//      val pn = new p.ParseQuotedValue
//
//      pn.read mustBe Some("blammo_&|")
//      p.next mustBe Some('\n')
//    }
//  }
//}
