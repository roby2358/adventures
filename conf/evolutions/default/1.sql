-- Users schema

-- !Ups

CREATE TABLE Character (
    id varchar(20) NOT NULL,
    name varchar(255) NOT NULL,
    food bigint NOT NULL,
    hunger bigint NOT NULL,
    power bigint NOT NULL,
    stamina bigint NOT NULL,
    PRIMARY KEY (id)
);

-- !Downs

DROP TABLE Character;