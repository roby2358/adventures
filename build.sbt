val scala3Version = "3.0.0-M3"

lazy val root = project
  .in(file("."))
  // Port https://github.com/playframework/playframework/tree/master/
  // dev-mode/sbt-plugin/src
  .enablePlugins(PlayScala)
  .settings(
    name := "adventures",
    organization := "org.yuwakisa",
    version := "1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    useScala3doc := true,

    scalacOptions ++= Seq(
      "-language:implicitConversions",
//      "-old-syntax",
//      "-new-syntax",
      "-indent",
      "-Yindent-colons"
      //      "-rewrite"
    ),

    libraryDependencies ++= Seq(
      guice,
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
      "com.h2database" % "h2" % "1.4.200",
      evolutions,
      jdbc,
      "org.playframework.anorm" %% "anorm" % "2.6.7",

//      "com.typesafe.play" %% "twirl-api" % "1.5.0",
//      "com.typesafe.play" %% "play-server" % "2.8.7",
//      "com.typesafe.play" %% "play-akka-http-server" % "2.8.7",
//      "com.typesafe.play" %% "filters-helpers" % "2.8.7",
//      "com.typesafe.play" %% "play-logback" % "2.8.7",
//      "com.typesafe.play" %% "filters-helpers" % "2.8.7",
//      "com.typesafe.play" %% "play-logback" % "2.8.7"
    ),
//      .map(_.withDottyCompat(scalaVersion.value)),

    libraryDependencies := libraryDependencies.value
      .map(_.withDottyCompat(scalaVersion.value))

    // Adds additional packages into Twirl
    //TwirlKeys.templateImports += "org.yuwakisa.controllers._"

    // Adds additional packages into conf/routes
    // play.sbt.routes.RoutesKeys.routesImport += "org.yuwakisa.binders._"
  )