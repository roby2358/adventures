$.village = function(){

  function showOnly(id) {
    $('.village').hide();
    $(id).show();
  }

  $('.village').hide();

  $('#villageChiefButton').click(() => { showOnly('#villageChief'); });
  $('#villageSpiritHutButton').click(() => { showOnly('#villageSpiritHut'); });
  $('#villageCraftingButton').click(() => { showOnly('#villageCrafting'); });
  $('#villageHutButton').click(() => { showOnly('#villageHut'); });

  console.log('village ready!');
}();
