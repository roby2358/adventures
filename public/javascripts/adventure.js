$.Adventure = function() {

  function svgElement(tag) {
    return document.createElementNS('http://www.w3.org/2000/svg', tag);
  }

  function drawCircle(id,x,y,r,color) {
    var dot = $(svgElement('circle'))
        .attr('id',id)
        .attr('cx', x)
        .attr('cy', y)
        .attr('r', r)
        .attr('fill', color)
        .attr('stroke', 'black')
        .attr('stroke-width', '5');
    dot.appendTo($("svg#map"));
    return dot;
  };

  // where to position the center dot
  var mapx = 400;
  var mapy = 300;
  var colors = ['blue', '#00C000', 'green', '#CC6600', 'brown'];

  function goto(id) {
    $("circle").remove();

    var c = $.graph.nodes[id];

    $('image#mapimage').attr('x', mapx - c.x);
    $('image#mapimage').attr('y', mapy - c.y);

    drawCircle(id, mapx, mapy, 15, 'white');

    $.graph.edges
      .filter(e => e[0] == id || e[1] == id)
      .map(e => e[0] == id ? e[1] : e[0])
      .forEach(i => {
        var d = $.graph.nodes[i];
        if (d.color > 0) {
          var dot = drawCircle(
            i,
            mapx + (d.x - c.x),
            mapy + (d.y - c.y),
            15,
            colors[d.color]);
          dot.click(() => goto(i));
        }
      });
  }

  goto(200);

  console.log('adventure ready!');
};
