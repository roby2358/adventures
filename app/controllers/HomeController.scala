package controllers

import anorm.SqlParser._
import anorm._
import models.Character
import play.api.db.Database
import play.api.mvc._

import java.sql.Connection
import javax.inject._
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(
                                val controllerComponents: ControllerComponents,
                                db: Database,
                                databaseExecutionContext: DatabaseExecutionContext
                              )extends BaseController :

  val characterId = "1234"

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index(): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      val fc = createCharacter(Character(characterId, "Hero", 100, 100, 100, 100))
      Await.result(fc, 10.seconds)
      val character = Await.result(readCharacter(characterId), 10.seconds)
      Ok(views.html.index(character))
    }

  def profile(): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      val character = Await.result(readCharacter(characterId), 10.seconds)
      Ok(views.html.profile(character))
    }

  def village(): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      val character = Await.result(readCharacter(characterId), 10.seconds)
      Ok(views.html.village(character))
    }

  def adventure(): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      val character = Await.result(readCharacter(characterId), 10.seconds)
      Ok(views.html.adventure(character))
    }

  def doSql[A](f: Connection => A): Future[A] =
    Future {
      db.withConnection { conn => f(conn) }
    }(databaseExecutionContext)

  def createCharacter(c: Character): Future[Unit] =
    doSql[Unit] { implicit conn =>
      val q = "MERGE INTO Character VALUES ({id}, {name}, {food}, {hunger}, {power}, {stamina})"
      SQL(q).on("id" -> c.id,
        "name" -> c.name,
        "food" -> c.food,
        "hunger" -> c.hunger,
        "power" -> c.power,
        "stamina" -> c.stamina)
        
        .executeUpdate()
    }

  val simple: RowParser[Character] =
    get[String]("id") ~
      get[String]("name") map {
      case id ~ name => Character(id, name, 100, 100, 100, 100)
    }

  def readCharacter(id: String): Future[Character] =
    doSql[Character] { implicit conn =>
      //      val q = s"select * from Character where id='$id'"
      val q = "select * from Character where id = {id}"
      SQL(q).on("id" -> id).as(simple.single)
    }
