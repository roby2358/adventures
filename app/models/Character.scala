package models

/**
 * Players create and play characters in the game
 *
 * @param id
 * @param name Character name
 * @param status Social standing
 * @param wealth Wealth level
 * @param skills List of acquired skills
 * @param maxSkills The maximum number of skills the player may have
 */
case class Character(id: String,
                     name: String,
                     food: Int,
                     hunger: Int,
                     power: Int,
                     stamina: Int)
