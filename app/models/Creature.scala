package models

object Creature {

  val Creatures = Seq(
    Creature("Squirrel", "Squirrel", 1, 1, 1),
    Creature("Deer", "Deer", 3, 3, 3),
    Creature("Gazelle", "Gazelle", 4, 3, 3),
    Creature("Hyena", "Hyena", 5, 2, 2),
    Creature("Wolf", "Wolf", 6, 2, 2),
    Creature("Bison", "Bison", 8, 6, 6),
    Creature("Aurochs", "Aurochs", 9, 7, 6),
    Creature("Bear", "Bear", 12, 5, 5),
    Creature("Tiger", "Tiger", 14, 5, 5),
    Creature("Lion", "Lion", 16, 6, 6),
    Creature("Stegosaurus", "Stegosaurus", 14, 12, 12),
    Creature("Velociraptor", "Velociraptor", 16, 10, 5),
    Creature("Tyrannosaurus Rex", "Tyrannosaurus Rex", 20, 20, 12),
    Creature("Brachiosaurus", "Brachiosaurus", 8, 100, 30),
  )
}

case class Creature(id: String,
                    name: String,
                    power: Int,
                    hits: Int,
                    food: Int)
