package models

import scala.util.Random

object Skill {
  val Skills = Seq(
    "Repair",
    "Healing",
    "Cooking",
    "Fermenting",
    "Bartering",
    "Racing",
    "Gambling",
    "Transport",
    "Scouting",
    "Teaching",
    "Weapon Making",
    "Fishing",
    "Hunting",
    "Gathering",
    "Hiding",
    "Spear",
    "Club",
    "Tailoring",
    "Crafting",
    "Dyeing",
    "Building",
    "Singing",
    "Dancing",
    "Storytelling",
    "Fast Talking",
    "Drum",
    "Pipes",
    "Spirit Vision",
    "Farming",
    "Herding",
  )
}

case class Skill(id: String,
                 name: String,
                 level: Int) {

  /**
   * See if a normally distributed roll with 90% chance of +/- 3 is greather
   * than or equal to challenge
   *
   * 84% chance if level == challenge; 95% chance of -1 to +3; 99% of -2 to +4
   *
   * @param challenge the level of the challenge
   * @return true if the level is equal to or higher than challenge
   */
  def test(challenge: Int): Boolean =
    level + Random.nextGaussian() + 1d >= challenge

  /**
   * In a head-to-head match against another's skill, compare the skills + a
   * normal random
   */
  def `match`(challenge: Int): Boolean =
    level + Random.nextGaussian() - challenge - Random.nextGaussian() >= 0

}
