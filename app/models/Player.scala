package models

/**
 * A player represents and actual human who may have many characters
 * @param id id
 * @param name visible player name
 * @param email the player's email
 */
case class Player(id: String,
                  name: String
                  ) {
}
