package nestdata

/**
 * Model the Forth loop construct of pre-test-post
 */
class Looop[A](pre: A => A, test: A => Boolean, post: A => A) :

  def apply(a: A): A =
    var ok = true
    var aa = a
    while (ok) do
      aa = pre(aa)
      ok = test(aa)
      if (ok) aa = post(aa)
    aa
