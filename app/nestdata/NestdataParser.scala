package nestdata


import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

object NestdataParser :
  type NestedData = Map[String, _]

  trait ParserState[A] :
    def read: Option[A]

class NestdataParser(var in: Source) :

    import NestdataParser._

    val WHITESPACE_CHARS: Set[Char] =
      Set(' ', '\n', '\t')

    val NAME_CHARS: Set[Char] =
      Seq('a' to 'z', 'A' to 'Z', '0' to '9', Seq('-', '_'))
        .flatMap(_.toSeq)
        .toSet

    val VALUE_END_CHARS: Set[Char] =
      Set(',', '-', '\n')

    var state = new Start
    var nesting = 0

    def read(): NestedData =
      state.read
      null

    var next: Option[Char] = in.nextOption

    def consume(): Boolean =
      next = next.flatMap({ _ => in.nextOption })
      next.isDefined

    class Start extends ParserState[Unit] :
      override def read: Option[Unit] =
        if next.isEmpty then None else Some(())

    class ParseWhitespace extends ParserState[Unit] :
      override def read: Option[Unit] =
        while next.exists(WHITESPACE_CHARS.contains) do
          consume()

        if next.isEmpty then None else Some(())

    class ParseName extends ParserState[String] :
      var name: mutable.ArrayBuffer[Char] = new ArrayBuffer[Char]()

      override def read: Option[String] =
        while next.exists(NAME_CHARS.contains) do
          name += next.get
          consume()
        if name.isEmpty then None else Some(name.mkString)

    class ParseValue extends ParserState[String] :
      var value: mutable.ArrayBuffer[Char] = new ArrayBuffer[Char]()

      override def read: Option[String] =
        while !next.exists(VALUE_END_CHARS.contains) do
          value += next.get
          consume()
        if value.isEmpty then None else Some(value.mkString.trim)

    class ParseQuotedValue extends ParserState[String] :
      var value: mutable.ArrayBuffer[Char] = new ArrayBuffer[Char]()

      override def read: Option[String] =
        val quote = next.get
        consume()
        while next.exists(c => quote != c && '\n' != c) do
          value += next.get
          consume()
        if next.exists(_ == quote) then consume()
        if value.isEmpty then None else Some(value.mkString.trim)

    class ParseArray extends ParserState[Seq[String]] :
      override def read: Option[Seq[String]] = None

    class ParseObject extends ParserState[NestedData] :
      override def read: Option[NestedData] = None
